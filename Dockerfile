FROM debian:buster-slim
LABEL maintainer="saltyvagrant@gmail.com"

ARG TARGETARCH
ARG TARGETVARIANT

ENV ACTIVATION_CODE abcdef
ENV LOCATION smart

ARG APP=expressvpn_3.0.2.12-1_

RUN apt-get update && apt-get install -y --no-install-recommends \
    ca-certificates expect iproute2 curl \
    && rm -rf /var/lib/apt/lists/* \
    && ARCH=$TARGETARCH \
    && if [ "$ARCH" = "arm" ] && [ "$TARGETVARIANT" = "v7" ]; then ARCH="armhf"; fi \
    && echo "Image ${APP}${ARCH}" \
    && curl -k -L "https://download.expressvpn.xyz/clients/linux/${APP}${ARCH}.deb" -o "/tmp/${APP}" \
    && dpkg -i "/tmp/${APP}" \
    && rm -rf "/tmp/${APP}"

COPY scripts/* /

ENTRYPOINT ["/bin/bash", "/entrypoint.sh"]
